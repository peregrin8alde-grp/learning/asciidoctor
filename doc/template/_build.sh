#!/bin/sh

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")" && pwd)
PARENT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")/.." && pwd)

OUTPUT_DIR=_build
DOCBASE_DIR="${SCRIPT_DIR}"

STYLE=material-red
#OPT_STYLE="-a stylesdir="${DOCBASE_DIR}/com/styles" -a stylesheet=${STYLE}.css"

rm -rf ${OUTPUT_DIR}

asciidoctor \
  -r asciidoctor-diagram \
  -D ${OUTPUT_DIR} \
  -R ./ \
  -a comdir="${DOCBASE_DIR}/com" \
  -a docinfo=shared \
  -a docinfodir="${DOCBASE_DIR}/com" \
  ${OPT_STYLE} \
  "**/index.adoc"

# plantuml を使うと謎のディレクトリが作成されるため、削除
if [ -d ${SCRIPT_DIR}/\? ]; then
  rm -rf ${SCRIPT_DIR}/\?/
fi


exit 0
