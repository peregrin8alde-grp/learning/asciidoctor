# ドキュメント

## コンテナ起動用ラッパー

Asciidoctor をインストールする代わりに Docker コンテナ起動用ラッパーを用意する。
実行時のカレントディレクトリの外はホスト側データを参照できないことに注意。

```
sudo tee /usr/local/bin/asciidoctor <<'EOF' >> /dev/null
#!/bin/sh
if [ -p /dev/stdin ]; then
  cat - | docker run \
    --rm \
    -u $(id -u):$(id -g) \
    -i \
    -v "$(pwd):$(pwd)" \
    -w "$(pwd)" \
    -e TZ=Asia/Tokyo \
    -e XDG_CACHE_HOME=/tmp \
    asciidoctor/docker-asciidoctor \
      asciidoctor "$@"
else
  docker run \
    --rm \
    -u $(id -u):$(id -g) \
    -i \
    -v "$(pwd):$(pwd)" \
    -w "$(pwd)" \
    -e TZ=Asia/Tokyo \
    -e XDG_CACHE_HOME=/tmp \
    asciidoctor/docker-asciidoctor \
      asciidoctor "$@"
fi
EOF
sudo chmod a+x /usr/local/bin/asciidoctor
```

```
sudo tee /usr/local/bin/asciidoctor-pdf <<'EOF' >> /dev/null
#!/bin/sh
if [ -p /dev/stdin ]; then
  cat - | docker run \
    --rm \
    -u $(id -u):$(id -g) \
    -i \
    -v "$(pwd):$(pwd)" \
    -w "$(pwd)" \
    -e TZ=Asia/Tokyo \
    -e XDG_CACHE_HOME=/tmp \
    asciidoctor/docker-asciidoctor \
      asciidoctor-pdf "$@"
else
  docker run \
    --rm \
    -u $(id -u):$(id -g) \
    -i \
    -v "$(pwd):$(pwd)" \
    -w "$(pwd)" \
    -e TZ=Asia/Tokyo \
    -e XDG_CACHE_HOME=/tmp \
    asciidoctor/docker-asciidoctor \
      asciidoctor-pdf "$@"
fi
EOF
sudo chmod a+x /usr/local/bin/asciidoctor-pdf
```

`XDG_CACHE_HOME` については https://github.com/asciidoctor/docker-asciidoctor/issues/470 を参照。
`Fontconfig error: No writable cache directories` が発生するのを避けるためにキャッシュ用ディレクトリと
してどのユーザでもアクセスできるディレクトリを指定している。


## ビルド方法

```
rm -rf _build

# html
asciidoctor \
  -r asciidoctor-diagram \
  -D _build \
  -R ./ \
  -a revdate! -a revremark! -a revnumber! \
  -a stylesdir=$(pwd)/styles/asciidoctor-skins-gh-pages/css -a stylesheet=material-blue.css \
  "**/index.adoc"

# `:data-uri:` 指定で画像を埋め込まない場合は変換後の出力先にも画像配置が必要？
#find . -name "_images" | grep -v "_build" | sed -e "s|_images||g" | xargs -I{} \
#  cp -rpf {}_images ./_build/{}_images

# pdf
## pdf テーマ : https://docs.asciidoctor.org/pdf-converter/latest/theme/
### テーマ適用 : https://docs.asciidoctor.org/pdf-converter/latest/theme/apply-theme/
### 日本語対応 : https://docs.asciidoctor.org/pdf-converter/latest/theme/font-support/
### テーマサンプル : https://github.com/asciidoctor/asciidoctor-pdf/tree/main/data/themes
asciidoctor-pdf \
  -a scripts=cjk \
  -a pdf-theme=default-with-font-fallbacks \
  -r asciidoctor-diagram \
  -D _build \
  -R ./ \
  "**/index.adoc"

## pdf テーマカスタマイズ
### https://docs.asciidoctor.org/pdf-converter/latest/theme/extend-theme/
asciidoctor-pdf \
  -a scripts=cjk \
  -a pdf-themesdir=template/com/pdftheme \
  -a pdf-theme=custumize \
  -r asciidoctor-diagram \
  -D _build \
  -R ./template \
  "**/index.adoc"
```
