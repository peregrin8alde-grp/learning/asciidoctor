# Asciidoctor Diagram 向けツールのインストール

Asciidoctor の (https://github.com/asciidoctor/docker-asciidoctor)[Docker イメージ]には ERD / Graphviz 以外がインストールされていないため、事前インストールが必要

> Asciidoctor Diagram 2.3.0 with ERD and Graphviz integration (supports plantuml and graphiz diagrams)

Docker イメージは alpine ベースのため、そこに追加する想定

Gnuplot は以下でインストール

```
apk add --no-cache \
  gnuplot
```

Mermaid は js を使ったほうが早い？

```
VERSION=1.68
docker build -t asciidoctor:${VERSION}-with-gnuplot --build-arg asciidoctor_version=${VERSION} ./
```

